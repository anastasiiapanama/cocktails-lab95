const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const User = require("./models/User");
const Cocktail = require('./models/Cocktail');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [User1, Admin] = await User.create({
        email: 'user@1',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User'
    }, {
        email: 'admin@1',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin'
    });

    await Cocktail.create({
        user: User1,
        title: 'Kiwi Lime and Mint Magic Julep',
        image: 'cocktails/img2.jpeg',
        recipe: 'I mixed kiwifruit and lime juice to create the base of this amazing drink. Then it’s muddled with some mint (and a dash of mint liqueur). Next, fill the glasses with crushed ice (or half-filled with crushed magic ice if you’re not using color changing alcohol). Add extra lime juice, followed by color changing vodka (or regular vodka) and club soda to make this Magic Julep. Top with more crushed magic ice. The drink transitions from green at the bottom to pink, purple and blue at the top! Almost like a mermaid magic julep! 🙂',
        published: false,
        ingredients: [
            {title: 'Kiwifruit', amount: '50ml'},
            {title: 'Lime juice', amount: '30ml'},
            {title: 'Mint liqueur', amount: '9cl'},
        ]
    }, {
        user: User1,
        title: 'LEMON-LIME MAGIC MARGARITA',
        image: 'cocktails/img1.jpeg',
        recipe: 'I mixed kiwifruit and lime juice to create the base of this amazing drink. Then it’s muddled with some mint (and a dash of mint liqueur). Next, fill the glasses with crushed ice (or half-filled with crushed magic ice if you’re not using color changing alcohol). Add extra lime juice, followed by color changing vodka (or regular vodka) and club soda to make this Magic Julep. Top with more crushed magic ice. The drink transitions from green at the bottom to pink, purple and blue at the top! Almost like a mermaid magic julep! 🙂',
        published: true,
        ingredients: [
            {title: 'Kiwifruit', amount: '50ml'},
            {title: 'Lime juice', amount: '30ml'},
            {title: 'Mint liqueur', amount: '9cl'},
        ]
    }, {
        user: Admin,
        title: 'LEMON-LIME MAGIC MARGARITA',
        image: 'cocktails/img3.jpeg',
        recipe: 'I mixed kiwifruit and lime juice to create the base of this amazing drink. Then it’s muddled with some mint (and a dash of mint liqueur). Next, fill the glasses with crushed ice (or half-filled with crushed magic ice if you’re not using color changing alcohol). Add extra lime juice, followed by color changing vodka (or regular vodka) and club soda to make this Magic Julep. Top with more crushed magic ice. The drink transitions from green at the bottom to pink, purple and blue at the top! Almost like a mermaid magic julep! 🙂',
        published: true,
        ingredients: [
            {title: 'Kiwifruit', amount: '50ml'},
            {title: 'Lime juice', amount: '30ml'},
            {title: 'Mint liqueur', amount: '9cl'},
        ]
    });

    await mongoose.connection.close();
};

run().catch(console.error);