const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public'),
    db: {
        url: 'mongodb://localhost/cocktails',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        },
    },
    facebook: {
        appId: '1621345674721871',
        appSecret: '900609a2b12d161717581c301a4d212b',
    },
    google: {
        clientId: '897366196277-uvb9vt4779pverbthcdmkv56h4rrj430.apps.googleusercontent.com'
    }
};