const express = require('express');

const Cocktail = require("../models/Cocktail");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const upload = require('../multer').cocktails;

const router = express.Router();

router.get('/all', async (req, res) => {
    try {
        const criteria = {};

        if(req.query.user) {
            criteria.user = req.query.user;
        }

        const cocktails = await Cocktail.find(criteria).populate('user', '_id');

        res.send(cocktails);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/my-cocktails', auth, permit('user'), async (req, res) => {
   try {
       const criteria = {user: req.user._id};

       const cocktails = await Cocktail.find(criteria).populate('user');

       res.send(cocktails);
   }  catch (e) {
       res.sendStatus(500);
   }
});

router.get('/:id', async (req, res) => {
    try {
        const cocktail = await Cocktail.findOne({_id: req.params.id});

        if (cocktail) {
            res.send(cocktail)
        } else (
            res.sendStatus(404)
        )

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
    try {
        const cocktailRequestData = req.body;
        cocktailRequestData.user = req.user._id;
        cocktailRequestData.ingredients = JSON.parse(req.body.ingredients);

        if (req.file) {
            cocktailRequestData.image = req.file.filename;
        }

        const cocktail = new Cocktail(cocktailRequestData);
        await cocktail.save();

        res.send({message: 'Your cocktail is being moderated...', cocktail});
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);
        cocktail.published = true;
        await cocktail.save();

        res.send(cocktail);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const reqEvent = req.body;
        reqEvent.user = req.user._id;

        if (reqEvent.user) {
            const event = await Cocktail.findOne({_id: req.params.id}).remove();
            res.send(event);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;