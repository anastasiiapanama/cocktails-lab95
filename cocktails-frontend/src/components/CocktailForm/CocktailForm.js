import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../UI/Form/FileInput";
import Button from "@material-ui/core/Button";

const CocktailForm = ({onSubmit, error, loading}) => {
    const [state, setState] = useState({
        title: '',
        image: '',
        recipe: ''
    });
    const [ingredients, setIngredients] = useState([{title: '', amount: ''}]);

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {

            if(key === 'ingredients') {
                state[key] = JSON.stringify(state[key]);
            }

            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const changeIngredient = (i, name, value) => {
        setIngredients(prev => {
            const ingCopy = {
                ...prev[i],
                [name]: value
            }

            return prev.map((ing, idx) => {
                if (idx === i) {
                    return ingCopy;
                }

                return ing;
            })
        });

        setState(prev => ({
            ...prev,
            ingredients: ingredients
        }));
    };

    const addIngredient = () => {
        setIngredients(prev => [...prev, {title: '', amount: ''}]);
    };

    const removeIngredient = i => {
        const index = ingredients.findIndex(ing => ing.i === i);
        const ingCopy = [...ingredients];
        ingCopy.splice(index, 1);
        setIngredients(ingCopy);
    }

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form onSubmit={submitFormHandler} noValidate>
            <Grid container direction="column" spacing={2}>

                <FormElement
                    required
                    label="Name"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                {ingredients.map((ing, i) => (
                    <Grid item container key={i} spacing={2}>
                        <FormElement
                            required
                            label="Title"
                            name="title"
                            value={ingredients.title}
                            onChange={e => changeIngredient(i, 'title', e.target.value)}
                        />

                        <FormElement
                            required
                            label="Amount"
                            name="amount"
                            value={ingredients.amount}
                            onChange={e => changeIngredient(i, 'amount', e.target.value)}
                        />

                        <Button onClick={() => removeIngredient(i)}>X</Button>
                    </Grid>
                ))}
                <Grid item xs>
                    <Button onClick={addIngredient}>Add ingredient</Button>
                </Grid>

                <FormElement
                    multiline
                    rows={3}
                    label="Recipe"
                    name="recipe"
                    value={state.recipe}
                    onChange={inputChangeHandler}
                />

                <Grid item xs>
                    <FileInput
                        name="image"
                        label="Image"
                        onChange={fileChangeHandler}
                        error={getFieldError('image')}
                    />
                </Grid>

                <Grid item xs>
                    <ButtonWithProgress
                        type="submit" color="primary" variant="contained"
                        loading={loading} disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default CocktailForm;