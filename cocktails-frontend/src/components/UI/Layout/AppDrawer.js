import React from 'react';
import {Link, useParams} from "react-router-dom";
import {Drawer, makeStyles, MenuItem, MenuList, Toolbar} from "@material-ui/core";
import {useSelector} from "react-redux";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
}));

const AppDrawer = () => {
    const params = useParams();
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    return (
        <Drawer
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
        >
            <Toolbar/>
            <MenuList>
                <MenuItem
                    component={Link}
                    to="/"
                    selected={!params.id}
                >
                    All cocktails
                </MenuItem>

                {user?.role === 'user' && (
                    <MenuItem
                        component={Link}
                        to="/my-cocktails"
                    >
                        My cocktails
                    </MenuItem>
                )}
            </MenuList>
        </Drawer>
    );
};

export default AppDrawer;