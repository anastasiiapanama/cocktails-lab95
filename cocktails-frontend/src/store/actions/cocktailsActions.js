import axiosApi from "../../axios-api";
import {historyPush} from "./historyaActions";
import {NotificationManager} from "react-notifications";

export const FETCH_COCKTAILS_REQUEST = 'FETCH_COCKTAILS_REQUEST';
export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAILS_FAILURE = 'FETCH_COCKTAILS_FAILURE';

export const FETCH_MY_COCKTAILS_REQUEST = 'FETCH_MY_COCKTAILS_REQUEST';
export const FETCH_MY_COCKTAILS_SUCCESS = 'FETCH_MY_COCKTAILS_SUCCESS';
export const FETCH_MY_COCKTAILS_FAILURE = 'FETCH_MY_COCKTAILS_FAILURE';

export const CREATE_COCKTAIL_REQUEST = 'CREATE_COCKTAIL_REQUEST';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_SUCCESS';
export const CREATE_COCKTAIL_FAILURE = 'CREATE_COCKTAIL_FAILURE';

export const DELETE_COCKTAIL_SUCCESS = 'DELETE_COCKTAIL_SUCCESS';

export const FETCH_SINGLE_COCKTAIL_SUCCESS = 'FETCH_SINGLE_COCKTAIL_SUCCESS';

export const PUBLISH_COCKTAIL_REQUEST = 'PUBLISH_COCKTAIL_REQUEST';
export const PUBLISH_COCKTAIL_SUCCESS = 'PUBLISH_COCKTAIL_SUCCESS';
export const PUBLISH_COCKTAIL_FAILURE = 'PUBLISH_COCKTAIL_FAILURE';

export const fetchCocktailsRequest = () => ({type: FETCH_COCKTAILS_REQUEST});
export const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, cocktails});
export const fetchCocktailsFailure = () => ({type: FETCH_COCKTAILS_FAILURE});

export const fetchMyCocktailsRequest = () => ({type: FETCH_MY_COCKTAILS_REQUEST});
export const fetchMyCocktailsSuccess = cocktails => ({type: FETCH_MY_COCKTAILS_SUCCESS, cocktails});
export const fetchMyCocktailsFailure = () => ({type: FETCH_MY_COCKTAILS_FAILURE});

export const createCocktailRequest = () => ({type: CREATE_COCKTAIL_REQUEST});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const createCocktailFailure = error => ({type: CREATE_COCKTAIL_FAILURE, error});

export const deleteCocktailSuccess = id => ({type: DELETE_COCKTAIL_SUCCESS, id});

export const fetchSingleCocktailSuccess = cocktail => ({type: FETCH_SINGLE_COCKTAIL_SUCCESS, cocktail});

export const publishCocktailRequest = () => ({type: PUBLISH_COCKTAIL_REQUEST});
export const publishCocktailSuccess = cocktail => ({type: PUBLISH_COCKTAIL_SUCCESS, cocktail});
export const publishCocktailFailure = () => ({type: PUBLISH_COCKTAIL_FAILURE});

export const fetchCocktails = () => {
    return async dispatch => {
        try {
            let url = '/cocktails/all';

            dispatch(fetchCocktailsRequest());
            const response = await axiosApi.get(url);

            dispatch(fetchCocktailsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCocktailsFailure());
            NotificationManager.error('Could not get cocktails list');
        }
    }
}

export const fetchMyCocktails = () => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(fetchMyCocktailsRequest());
            const response = await axiosApi.get('/cocktails/my-cocktails', {headers});
            dispatch(fetchMyCocktailsSuccess(response.data));
        } catch (e) {
            dispatch(fetchMyCocktailsFailure());
            NotificationManager.error('Could not get cocktails list');
        }
    }
}

export const createCocktail = cocktailData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(createCocktailRequest());
            await axiosApi.post('/cocktails', cocktailData, {headers});
            dispatch(createCocktailSuccess());
            dispatch(historyPush('/'));

            NotificationManager.success('Cocktail created successfully');
        } catch (e) {
            dispatch(createCocktailFailure(e.response.data));
            NotificationManager.error('Could not create new cocktail');
        }
    }
}

export const deleteCocktail = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            await axiosApi.delete('/cocktails/' + id, {headers});
            dispatch(deleteCocktailSuccess(id));
            dispatch(fetchCocktails());

            NotificationManager.success('Cocktail deleted successfully');
        } catch (e) {
            NotificationManager.error('Could not delete cocktail');
        }
    }
}

export const fetchSingleCocktail = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/cocktails/' + id);

            dispatch(fetchSingleCocktailSuccess(response.data));
        } catch (e) {
            NotificationManager.error('Could not get cocktail info');
        }
    }
}

export const publishCocktail = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(publishCocktailRequest());
            const response = await axiosApi.post('/cocktails/' + id, '', {headers});
            dispatch(publishCocktailSuccess(response.data));
            dispatch(fetchCocktails());

            NotificationManager.success('Cocktail publish successfully');
        } catch (e) {
            dispatch(publishCocktailFailure());
            NotificationManager.error('Could not publish cocktail');
        }
    }
}