import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import usersReducer, {initialState} from "./reducers/usersReducer";
import cocktailsReducer from "./reducers/cocktailsReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";

const rootReducer = combineReducers({
    cocktails: cocktailsReducer,
    users: usersReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user
        }
    });
});

export default store;
