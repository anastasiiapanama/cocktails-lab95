import {
    CREATE_COCKTAIL_FAILURE,
    CREATE_COCKTAIL_REQUEST,
    CREATE_COCKTAIL_SUCCESS,
    DELETE_COCKTAIL_SUCCESS,
    FETCH_COCKTAILS_FAILURE,
    FETCH_COCKTAILS_REQUEST,
    FETCH_COCKTAILS_SUCCESS, FETCH_MY_COCKTAILS_FAILURE, FETCH_MY_COCKTAILS_REQUEST, FETCH_MY_COCKTAILS_SUCCESS,
    FETCH_SINGLE_COCKTAIL_SUCCESS, PUBLISH_COCKTAIL_FAILURE,
    PUBLISH_COCKTAIL_REQUEST,
    PUBLISH_COCKTAIL_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
    cocktails: [],
    singleCocktail: {},
    myCocktailsLoading: false,
    cocktailsLoading: false,
    createCocktailError: null,
    createCocktailLoading: false,
    publishCocktailLoading: false
}

const cocktailReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_REQUEST:
            return {...state, cocktailsLoading: true};
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, cocktailsLoading: false, cocktails: action.cocktails};
        case FETCH_COCKTAILS_FAILURE:
            return {...state, cocktailsLoading: false};
        case FETCH_MY_COCKTAILS_REQUEST:
            return {...state, myCocktailsLoading: true};
        case FETCH_MY_COCKTAILS_SUCCESS:
            return {...state, myCocktailsLoading: false, cocktails: action.cocktails};
        case FETCH_MY_COCKTAILS_FAILURE:
            return {...state, myCocktailsLoading: false};
        case CREATE_COCKTAIL_REQUEST:
            return {...state, createCocktailLoading: true};
        case CREATE_COCKTAIL_SUCCESS:
            return {...state, createCocktailLoading: false};
        case CREATE_COCKTAIL_FAILURE:
            return {...state, createCocktailLoading: false, createCocktailError: action.error};
        case DELETE_COCKTAIL_SUCCESS:
            return {...state, cocktails: state.cocktails.filter(c => c.id !== action.id)};
        case FETCH_SINGLE_COCKTAIL_SUCCESS:
            return {...state, singleCocktail: action.cocktail};
        case PUBLISH_COCKTAIL_REQUEST:
            return {...state, publishCocktailLoading: true};
        case PUBLISH_COCKTAIL_SUCCESS:
            return {...state, publishCocktailLoading: false, cocktails: action.cocktails};
        case PUBLISH_COCKTAIL_FAILURE:
            return {...state, publishCocktailLoading: false};
        default:
            return state;
    }
}

export default cocktailReducer;