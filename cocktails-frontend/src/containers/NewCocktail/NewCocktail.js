import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {Typography} from "@material-ui/core";
import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktail} from "../../store/actions/cocktailsActions";

const NewCocktail = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.cocktails.createCocktailError);
    const loading = useSelector(state => state.cocktails.createCocktailLoading);

    const onCocktailFormSubmit = async cocktailData => {
        dispatch(createCocktail(cocktailData));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new cocktail</Typography>
            </Grid>
            <Grid item xs>
                <CocktailForm
                    onSubmit={onCocktailFormSubmit}
                    loading={loading}
                    error={error}
                />
            </Grid>
        </Grid>
    );
};

export default NewCocktail;