import React from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useSelector} from "react-redux";
import {apiURL} from "../../config";

import imageNotAvailable from '../../assets/images/imageNotAvailable.png';
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import {
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Icon,
    IconButton,
    Typography
} from "@material-ui/core";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import DeleteIcon from '@material-ui/icons/Delete';
import CloudOffIcon from '@material-ui/icons/CloudOff';
import CloudDoneIcon from '@material-ui/icons/CloudDone';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 250,
    },
});

const CocktailItem = ({id, title, image, published, deleteButton, publishButton}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    if (published) {
        published = <Icon>
            <CloudDoneIcon/>
        </Icon>
    } else {
        {user?.role === 'admin' && (
            published = <IconButton onClick={publishButton}>
                <CloudOffIcon/>
            </IconButton>
        )}
        {user?.role === 'user' && (
            published = <Typography>Your cocktail is being moderated...</Typography>
        )}
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        image={cardImage}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <IconButton component={Link} to={'/cocktail/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                    {user?.role === 'admin' && (
                        <IconButton onClick={deleteButton}>
                            <DeleteIcon />
                        </IconButton>
                    )}
                    {published}
                </CardActions>
            </Card>
        </Grid>
    );
};

export default CocktailItem;