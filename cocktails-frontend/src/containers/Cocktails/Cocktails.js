import React, {useEffect} from 'react';
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import {CircularProgress, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {deleteCocktail, fetchCocktails, publishCocktail} from "../../store/actions/cocktailsActions";
import CocktailItem from "./CocktailItem";
import CocktailsLayout from "../../components/UI/Layout/CocktailsLayout";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Cocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const loading = useSelector(state => state.cocktails.createCocktailLoading);

    useEffect(() => {
        dispatch(fetchCocktails());
    }, [dispatch]);

    const onDeleteHandler = id => {
        dispatch(deleteCocktail(id));
    }

    const publishCocktailButton = id => {
        dispatch(publishCocktail(id));
    }

    return (
        <CocktailsLayout>
            <Grid item container justify="space-between" alignItems="center" spacing={3}>
                <Grid item>
                    <Typography variant="h4">Cocktails</Typography>
                </Grid>

                {user && (
                    <Grid item>
                        <Button color="primary" component={Link} to="/cocktail-new">Add new cocktail</Button>
                    </Grid>
                )}

                <Grid item container spacing={4}>
                    {user?.role === 'admin' && (
                        <>
                            {loading ? (
                                <Grid container justify="center" alignItems="center" className={classes.progress}>
                                    <Grid item>
                                        <CircularProgress/>
                                    </Grid>
                                </Grid>
                            ) : cocktails && cocktails.map(cocktail => (
                                <CocktailItem
                                    key={cocktail._id}
                                    id={cocktail._id}
                                    title={cocktail.title}
                                    image={cocktail.image}
                                    recipe={cocktail.recipe}
                                    published={cocktail.published}
                                    ingredients={cocktail.ingredients}
                                    deleteButton={() => onDeleteHandler(cocktail._id)}
                                    publishButton={() => publishCocktailButton(cocktail._id)}
                                />
                            ))}
                        </>
                    )}

                    {user?.role === 'user' && (
                        <>
                            {loading ? (
                                <Grid container justify="center" alignItems="center" className={classes.progress}>
                                    <Grid item>
                                        <CircularProgress/>
                                    </Grid>
                                </Grid>
                            ) : cocktails.map(cocktail => (
                                <React.StrictMode key={cocktail._id}>
                                    {cocktail.published ? (
                                        <CocktailItem
                                            key={cocktail._id}
                                            id={cocktail._id}
                                            title={cocktail.title}
                                            image={cocktail.image}
                                            recipe={cocktail.recipe}
                                            published={cocktail.published}
                                            ingredients={cocktail.ingredients}
                                            deleteButton={() => onDeleteHandler(cocktail._id)}
                                            publishButton={() => publishCocktailButton(cocktail._id)}
                                        />
                                    ) : null}
                                </React.StrictMode>
                            ))}
                        </>
                    )}

                    {user?.role === undefined && (
                        <Grid container justify="center" alignItems="center" className={classes.progress}>
                            <Grid item>
                                To watch cocktails list you need Log in or Register
                            </Grid>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </CocktailsLayout>
    );
};

export default Cocktails;