import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {Divider, List, ListItem, ListItemText, Typography} from "@material-ui/core";
import {fetchSingleCocktail} from "../../store/actions/cocktailsActions";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: theme.spacing(4, 0, 2),
    },
}));

const CocktailInfo = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [dense] = React.useState(false);
    const cocktail = useSelector(state => state.cocktails.singleCocktail);

    useEffect(() => {
        dispatch(fetchSingleCocktail(match.params.id));
    }, [dispatch, match.params.id]);

    return cocktail ? (
        <Grid container direction="column" spacing="2">
            <Grid item xs>
                <Typography variant="h5">{cocktail.title}</Typography>
            </Grid>
            {cocktail.image && (
                <Grid item xs>
                    <img src={'http://localhost:8000/uploads/' + cocktail.image}
                         alt={cocktail.title} style={{maxWidth: 200, maxHeight: 200}}
                    />
                </Grid>
            )}
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography variant="body1"><b>Recipe:</b> {cocktail.recipe}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs container direction="column" spacing={1}>
                {cocktail.ingredients ? cocktail.ingredients.map(ing => {
                    return (
                        <Grid item xs={12} md={6} key={ing._id}>
                            <Typography variant="h6" className={classes.title}>
                                Ingredient:
                            </Typography>
                            <div className={classes.demo}>
                                <List dense={dense}>
                                    <ListItem>
                                        <ListItemText
                                            primary={ing.title}
                                            secondary={'Title'}
                                        />
                                        <ListItemText
                                            primary={ing.amount}
                                            secondary={'Amount (cl)'}
                                        />
                                    </ListItem>
                                </List>
                            </div>
                        </Grid>
                    )
                }) : null}
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
        </Grid>
    ) : null;
};

export default CocktailInfo;