import React, {useEffect} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import {fetchMyCocktails} from "../../store/actions/cocktailsActions";
import Grid from "@material-ui/core/Grid";
import {CircularProgress, Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import CocktailItem from "../Cocktails/CocktailItem";
import CocktailsLayout from "../../components/UI/Layout/CocktailsLayout";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const MyCocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const loading = useSelector(state => state.cocktails.myCocktailsLoading);

    useEffect(() => {
        dispatch(fetchMyCocktails());
    }, [dispatch]);

    return (
        <CocktailsLayout>
            <Grid item container justify="space-between" alignItems="center" spacing={3}>
                <Grid item>
                    <Typography variant="h4">My cocktails</Typography>
                </Grid>

                {user && (
                    <Grid item>
                        <Button color="primary" component={Link} to="/cocktail-new">Add new cocktail</Button>
                    </Grid>
                )}

                <Grid item container spacing={4}>
                    {user?.role === 'user' && (
                        <>
                            {loading ? (
                                <Grid container justify="center" alignItems="center" className={classes.progress}>
                                    <Grid item>
                                        <CircularProgress/>
                                    </Grid>
                                </Grid>
                            ) : cocktails && cocktails.map(cocktail => (
                                <React.StrictMode key={cocktail._id}>
                                    <CocktailItem
                                        key={cocktail._id}
                                        id={cocktail._id}
                                        title={cocktail.title}
                                        image={cocktail.image}
                                        recipe={cocktail.recipe}
                                        published={cocktail.published}
                                        ingredients={cocktail.ingredients}
                                    />
                                </React.StrictMode>
                            ))}
                        </>
                    )}
                </Grid>
            </Grid>
        </CocktailsLayout>
    )
};

export default MyCocktails;