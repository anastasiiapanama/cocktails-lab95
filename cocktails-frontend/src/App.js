import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from 'react-router-dom';
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cocktails from "./containers/Cocktails/Cocktails";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import CocktailInfo from "./containers/CocktailInfo/CocktailInfo";
import MyCocktails from "./containers/MyCocktails/MyCocktails";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Cocktails}/>
          <Route path="/cocktail/:id" exact component={CocktailInfo}/>
          <Route path="/my-cocktails" exact component={MyCocktails}/>
          <ProtectedRoute
              path="/cocktail-new"
              exact
              component={NewCocktail}
              isAllowed={user}
              redirectTo="/login"
          />
          <Route path="/register" component={Register}/>
          <Route path="/login" component={Login}/>
        </Switch>
      </Layout>
  );
};

export default App;